﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VideoInfosEditor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int File_Count = 0;
                double File_Duration = 0;

                // Get working path
                Console.WriteLine("Paste folder path here...");
                var Directory_Name = Console.ReadLine();

                // Create file list
                string File_Infos_Creation = "Filename,";
                File_Infos_Creation += "Name,";
                File_Infos_Creation += "Id,";
                File_Infos_Creation += "Film date,";
                File_Infos_Creation += "Year,";
                File_Infos_Creation += "Length (s)\n";

                if (File.Exists(Directory_Name + "\\Files.txt"))
                {
                    File.Delete(Directory_Name + "\\Files.txt");
                }
                File.AppendAllText(Directory_Name + "\\Files.txt", File_Infos_Creation);

                // Get film
                string[] Films = System.IO.Directory.GetFiles(Directory_Name, "Film (*", SearchOption.TopDirectoryOnly);

                // Iterate trough files
                foreach (string Film in Films)
                {
                    // Get file name
                    string FileName = Path.GetFileNameWithoutExtension(Film);

                    // Parse file name
                    string[] words = FileName.Split('-');
                    string Id = words[0].Remove(words[0].Length - 1);
                    string Year = words[1].Remove(0, 1);
                    Year = Year.Remove(Year.Length - 1);
                    string Name = words[2].Remove(0, 1);

                    // Create date
                    DateTime Film_Date = new DateTime(Convert.ToInt32(Year), 01, 01, 00, 00, 00);

                    //Change the file created time.
                    File.SetCreationTime(Film, Film_Date);

                    // Create media file
                    TagLib.File File_Film = TagLib.File.Create(Film);

                    // Add title
                    File_Film.Tag.Title = Name;
                    File_Film.Tag.TitleSort = Id;

                    // Add year
                    File_Film.Tag.Year = Convert.ToUInt32(Year);

                    // Add comment
                    DateTime Comment_Date = DateTime.Now;
                    File_Film.Tag.Comment = Id + " - Scanned by Nicolas Toupin (nicotoup@outlook.com) in " + Comment_Date.Year + ".";

                    // Get video duration
                    double Length = File_Film.Properties.Duration.TotalSeconds;

                    string File_Infos_Fill = FileName + ",";
                    File_Infos_Fill += Name + ",";
                    File_Infos_Fill += Id + ",";
                    File_Infos_Fill += Film_Date.ToString() + ",";
                    File_Infos_Fill += Year + ",";
                    File_Infos_Fill += Length.ToString() + "\n";
                    File.AppendAllText(Directory_Name + "\\Files.txt", File_Infos_Fill);

                    File_Film.Save();

                    Console.WriteLine("Processed film : " + FileName);
                    ++File_Count;
                    File_Duration += Length;
                }

                Console.WriteLine("Processes total of " + File_Count.ToString() + " file(s).");

                // Ouput infos
                string Text = "Number of files : " + File_Count.ToString() + "\n";
                Text += "Total duration : " + File_Duration.ToString() + "\n";
                File.WriteAllText(Directory_Name + "\\Infos.txt", Text);
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            Console.WriteLine("Press any key to stop...");
            Console.ReadKey();
        }
    }
}